# Abstract

In the evolving landscape of Bitcoin Cash, Knuth has consistently been at the forefront. Building on our Q2 milestones, Q3 sees us taking a significant leap: integrating Knuth into the web browser. This move isn't just about convenience; it's about making the Knuth platform more accessible and functional for everyone in the BCH ecosystem.


# What we did in Q2

During the second quarter of 2023, we faced significant challenges and set out to fulfill the ambitious objectives we outlined in our previous campaign. We are proud to report on the concrete advances made and the groundwork we've laid for ongoing projects:

## At the Javascript API Level

1. **Event Subscription Implementation**: We rolled out a range of features that allow users to subscribe and receive real-time notifications about key events. This includes: Received Blocks, Reorgs and Received Transaction Notifications
2. **Initial Wallet Implementation**: Despite its comprehensive nature, we commenced the implementation phase for our Wallet, setting a solid foundation upon which we will continue building in the upcoming months.

## At the Node and Performance Level

1. **Database Restructuring**: We undertook a comprehensive overhaul of our database, optimizing its structure and significantly improving data processing efficiency.
2. **Significant Progress in the UTXO Set Database**: Although we recognized that this was a project that might span beyond a single quarter, we achieved remarkable progress, inching closer to a more robust and agile system.


## Other Achievements

1. **Launching of REST-API based on JS-API**: We unveiled a new interface for our users through the REST-API, grounded in our JS-API.
2. **Release of a Custom Conan Server**:  For those acquainted with the realms of C and C++, we rolled out our very own Conan Server, efficiently housing all the binaries and compiled versions of our components in these languages.
3. **Introduction of Knuth Labs**: Our R&D division focused on accelerating the P2P electronic cash revolution. We are delving into innovative data structures and hardware devices to amplify Bitcoin Cash's capabilities.
4. **Improvements in the Build System**: With an aim to stay ahead of the curve, we optimized our build system to support the latest architectures released by Intel.

We acknowledge that, while we've made significant strides, there are still outstanding tasks and challenges ahead. We are committed to persisting in our efforts and keeping our community informed about the progress and updates of these projects. Your trust drives us to excel and deliver on our promises, and we eagerly look forward to what the next quarter holds for us.


# Plans for Q3 and Beyond

As we move forward with our vision for Knuth, we continuously seek to improve and broaden our horizons. During Q3, we will dive deeper into the following challenges and opportunities:

## 1. Knuth JS-API in the Browser

Adaptability and flexibility have always been central to our project. Knuth's core is built in C++, and while its canonical C++ builds weren't designed with browsers in mind, we've identified a rising need within our community: the capability to operate Knuth directly in the browser.

Up to this point, our Javascript API has utilized NAN (Node Add-ons for Node.js) to enable communication between Javascript and C++. However, NAN doesn't support WebAssembly. Thus, we're faced with the challenge of transitioning our tools and capabilities to this new environment.

Porting modules and dependencies to WebAssembly is undoubtedly ambitious and intricate, but we are committed to making it happen. Our goal is for the Knuth JS-API to be the foundation upon which future Bitcoin Cash Wallets are built. We want Wallet developers to focus on crafting exceptional user experiences, resting assured that the underlying functionality is robust and reliable thanks to Knuth.

Our mission is clear: to advance, adapt, and consolidate Knuth as an essential tool in the Bitcoin Cash landscape.


## 2. Progress in Javascript API

1. **Wallet Expansion**: We aim to enhance our Wallet, adding cutting-edge tools and functionalities for our users.
2. **BIP-47 or Reusable Payment Addresses**: We will invest time researching both BIP-47 and Reusable Payment Addresses. With community feedback, we'll decide which technology (or perhaps an alternative) to incorporate into the Knuth JS-API.
3. **CashFusion Implementation**: We'll integrate CashFusion into JS-API, further expanding our tools' privacy and efficiency capabilities.
4. **Event Subscriptions**: We'll strengthen and broaden the subscription options, including notifications for Double-Spent proofs and other relevant events.
5. **Pseudo-Random Wallet Creation, Transaction Creation API, Transaction Signing, and Hardware Wallets Support**: We'll continue developing in all these areas to offer a more intuitive, secure, and versatile experience to our users.

## 3. Performance Improvements and Restructuring


1. **Smart UTXO Set Optimization**: We will develop an algorithm that distinguishes between hot and cold UTXOs, allowing for strategic allocation across different storage mediums based on usage frequency.
2. **Harnessing the GPU**: We're exploring the potential of storing a hot subset of the UTXO Set in a GPU's memory. This adaptation, aside from speeding up access, will enable us to harness the GPU's computational power for signature verification, requiring modifications to the existing code.
3. **Continued Research**: We'll delve into the concept of creating a hardware device that efficiently manages the UTXO Set.


# Scope

Our roadmap reflects a deep-seated commitment to Bitcoin Cash and its enthusiasts. This campaign, while ambitious, centers on a pivotal objective: introducing Knuth to web browsers. For the next three months, achieving this goal will remain at the forefront of our efforts. Keep in mind that while we aim for extensive strides, the scope of this campaign is shaped by the advancements we can realistically achieve within this period. Regardless, this milestone will set the stage for further developments and innovations in subsequent phases.

# Objective

The objective of this campaign is to raise 105 BCH to fund the development and execution of the enhancements detailed above. These resources will be instrumental in driving Knuth's progression over the next three months.

# Role of Knuth in the Bitcoin Cash Community

Knuth has always been at the forefront of the Bitcoin Cash journey. As one of the pioneering nodes, we didn't just support BCH — we were integral in its early implementation. While others have come and gone, or perhaps diminished in activity, Knuth remains steadfast and relentless. Our continuous dedication and unwavering commitment have made us, arguably, the most active project in the BCH community today. This isn't about claiming a superior effort but recognizing that our consistent and ongoing work is testament to our deep-rooted belief in Bitcoin Cash's potential and future.




<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>

# Thank you

To our dedicated community and staunch supporters, your belief fuels our drive. As we strive to elevate Bitcoin Cash's capabilities and outreach, your unwavering trust and feedback are our guiding lights. Here's to making Bitcoin Cash the future of P2P electronic cash, together.