Having a node ready to be used for mining as quickly as possible is one of our goals. That is why we want to implement UTXO commitments or any other technology that allows rapid node synchronization for use in mining. 

Estimated time: 480 hours.
