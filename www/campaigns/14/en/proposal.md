
In May 2023, the Bitcoin Cash network will undergo a hard fork that includes several important changes to the protocol, in addition to the implementation of CashTokens as a core feature. These changes include:

- Restrict Transaction Version
- Minimum Transaction Size
- CashTokens
- P2SH32

This means that all users and nodes on the BCH network will need to support these changes in order to continue participating in the network.

CashTokens are a new type of token that allow users to securely and transparently hold and transfer digital assets on the Bitcoin Cash (BCH) network.

Knuth is a full node implementation of BCH that aims to provide a user-friendly platform for developers. The Knuth project includes several language APIs, such as:

- The C++ API
- The C API
- The C# API
- The Javascript and TypeScript API
- The Python API

The goal of this campaign is to fund the integration of these changes into the Knuth node and libraries, making it easier for developers and users to access and utilize the updated features of the BCH network.

The success of this campaign is crucial for the future of the Knuth project, as it will allow us to continue developing and improving our products and services for the benefit of the Bitcoin Cash community.

# Objective


The objective of this campaign is to raise 120 BCH to fund the development and integration of these changes into the Knuth node and libraries in preparation for the May 2023 hard fork.

# Scope


The scope of this project includes the integration of the changes mentioned in the background section into the Knuth node.
This project is expected to take a total of 480 hours to complete.
The budget for this project has been calculated based on the estimated number of hours required and the hourly rate of the development team.


<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>


# Our plans, for 2023 and beyond


- Implement the changes needed to support the May 2023 BCH Hard Fork (this Flipstarter campaign).
- Maintain and improve our existing APIs: C++, C, C#, Javascript, TypeScript and Python.
- General maintenance of the node software.
- Implement UTXO Commitments.
- Development of the Knuth Rust Library.
- Development of the Knuth Java Library.
- Development of the Knuth Go-lang Library.
- Development of the Knuth Eiffel Library.

The items listed are all the tasks in our backlog. Rather than tackling all of these tasks at once in a large campaign, we prefer to approach them on a feature-by-feature basis. Once we complete the current campaign, we will move on to the next one. This approach allows us to focus on shorter tasks and also enables us to be flexible and responsive to the changing needs of the Bitcoin Cash ecosystem.

# Thank you!


Knuth is an open source software that relies on community support for its development. We are grateful for the generosity of our supporters and the entire Bitcoin Cash ecosystem. It is thanks to your support that we are able to pursue our shared goal of making Bitcoin Cash the peer-to-peer electronic cash system for the world.

---

# References

---

**Restrict Transaction Version (CHIP-2021-01 v1.0)**

https://gitlab.com/bitcoin.cash/chips/-/blob/3b0e5d55e1e139046794e850287b7acb795f4e66/CHIP-2021-01-Restrict%20Transaction%20Versions.md

See discussion here: https://bitcoincashresearch.org/t/restrict-transaction-version-numbers/

**Minimum Transaction Size (CHIP-2021-01 v0.4)**

https://gitlab.com/bitcoin.cash/chips/-/blob/00e55fbfdaacf1436e455289086d9b4c6b3e7306/CHIP-2021-01-Allow%20Smaller%20Transactions.md

See discussion here: https://bitcoincashresearch.org/t/chip-2021-01-allow-transactions-to-be-smaller-in-size/

**CashTokens (CHIP-2022-02 v2.2.1)**

https://github.com/bitjson/cashtokens

See discussion here: https://bitcoincashresearch.org/t/chip-2022-02-cashtokens-token-primitives-for-bitcoin-cash/

**P2SH32 (CHIP-2022-05 v1.5.1)**

https://gitlab.com/0353F40E/p2sh32/-/blob/f58ecf835f58555c9087c53af25da92a0e74534c/CHIP-2022-05_Pay-to-Script-Hash-32_(P2SH32)_for_Bitcoin_Cash.md

See discussion here: https://bitcoincashresearch.org/t/chip-2022-05-pay-to-script-hash-32-p2sh32-for-bitcoin-cash/
