The Knuth development team is working hard to release our first version of the Knuth C#/.NET library.

Knuth is a Bitcoin Cash full-node, but it's not just that.

Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem.

<p align="center"><img width="800px" src="https://github.com/k-nuth/cs-api/raw/master/docs/images/demo.png" /></p>

So the C#/.NET library is about to join our family of libraries: C++17 and C. But this does not end here, in the coming months we will be releasing the Knuth libraries for other programming languages: Python, Javascript, Java, Go, Eiffel, Rust.

<p align="center"><img width="800px" src="https://github.com/k-nuth/cs-api/raw/master/docs/images/Knuth%20Programming%20langs.png" /></p>

We will be presenting our C#/.NET library at the Bitcoin Cash DevCon III. 

We would like you to be able to use the Knuth node as a library in your favorite programming language.

---

Estimated time: 160 hours.
