# Abstract

We are embarking on a crucial development phase to ensure Knuth, our high-performance Bitcoin Cash node, and its comprehensive set of libraries, are fully compatible with the upcoming Bitcoin Cash network upgrade scheduled for May 2025. This upgrade will implement two significant CHIPs:

## 1. [CHIP-2021-05: Targeted Virtual Machine Limits (VM Limits)](https://github.com/bitjson/bch-vm-limits)

Proposes retargeting the virtual machine limits to enable more advanced contracts, reduce transaction sizes, and decrease full node computational requirements.

## 2. [CHIP-2024-07: BigInt](https://github.com/bitjson/bch-bigint)

Aims to enable high-precision arithmetic operations in the Bitcoin Cash virtual machine by removing the limit on the size of numbers. This is essential for financial and cryptographic applications requiring complex and precise calculations.


# What We Did in the Past

Over the past months, we have been diligently working on enhancing Knuth and its associated libraries to support the evolving needs of the Bitcoin Cash community:

- Creation of Knuth WebAssembly Library: We developed a new WebAssembly library, enabling developers to run Knuth directly in web browsers, expanding accessibility and ease of use.
- Implementation of ABLA: Successfully integrated the Adaptive Blocksize Limit Algorithm (ABLA) into Knuth Node, ensuring readiness for the May 2024 Bitcoin Cash upgrade and enhancing network adaptability.
- Significant Performance Improvements:
	* Initial Block Download (IBD): Optimized the IBD process, resulting in faster synchronization times and more efficient resource utilization.
	* Refactoring of Storage/DB Library: Undertook a complete refactoring of our storage and database libraries for the blockchain and UTXO Set, improving data handling and scalability.


# Knuth Projects Statistics

Delve into the core of Knuth development landscape with these insightful statistics, showcasing the breadth and depth of our work:

## Language-Specific

**C and C++**: Approximately `230,000` lines of code across `1,100` files.  
**C#**: `18,500` lines of code across `210` files.  
**Python**: `17,000` lines in `110` files  
**JavaScript**: `8,000` lines of code in `70` files.  
**TypeScript**: `1,100` lines of code in `40` files.  
**Other**: `57,000` lines in `252` files, which includes infrastructure files, continuous integration (CI) scripts, web components, and build systems.

---

**Total Repositories**: `28`.  
**Total Files**: `2,300`.  
**Total Lines of Code**: Approximately `362,000`.

These statistics paint a picture of a dynamic and diverse development environment at Knuth, underlining our commitment to building a comprehensive and robust Bitcoin Cash ecosystem.


# Plans for the Next Phase

## Implementing the CHIPs in Knuth Node and Libraries

- Upgrade Knuth Node and Libraries: Update our node and all associated libraries to fully support the new CHIPs.
- Comprehensive Testing: Conduct exhaustive tests to ensure stability, security, and performance in the post-upgrade environment.

## BigInt Implementation Strategy

Unlike Bitcoin Cash Node, which implemented BigInt using GMP (GNU Multiple Precision Arithmetic Library), we will be working with three different implementations to ensure robustness and security:

- GMP: While GMP is a well-known and robust library, there have been instances of miscomputations on some platforms in the past. We have expressed our concerns and detailed analysis in [our announcement](https://gitlab.com/fpelliccioni/bch-bigint-alternatives).
- Toccata: An alternative that is a rewrite of GMP in a formally verifiable language. Toccata has achieved formal verification and generated a C language library from this verified model, enhancing reliability. More information can be found on [their website](https://toccata.gitlabpages.inria.fr/toccata/gallery/multiprecision.en.html).
- Boost Multiprecision: We will also explore and implement Boost Multiprecision as another option for big number calculations.

## Continuous Maintenance and Improvements

- Regular Maintenance: Ongoing upkeep required for any software project operating within a Bitcoin Cash node.
- Performance Optimizations: Implement enhancements to improve efficiency and user experience.
- Expand Multilanguage Support: Ensure all our libraries in C++, C, C#, JavaScript, TypeScript, Python, and WebAssembly are updated and compatible with the new changes.


# Scope

Our roadmap for the next six months is focused on ensuring that Knuth Node and its libraries are fully prepared for the May 2025 Bitcoin Cash upgrade. By implementing the "VM Limits" and "BigInt" CHIPs, we aim to:

## Implement the CHIPs in Knuth Node and Libraries:

- Update our node and all associated libraries to fully support the new CHIPs.
- BigInt Implementation Strategy:
* Work with three different BigInt libraries—GMP, Toccata, and Boost Multiprecision—to ensure robustness and security.
* Address concerns about GMP by exploring alternatives like Toccata, which offers formal verification, and Boost Multiprecision for its reliability.
- Conduct exhaustive tests with all three BigInt implementations to ensure stability, security, and optimal performance.

## Continuous Maintenance and Improvements:

- Regular maintenance required for any software project operating within a Bitcoin Cash node.
- Performance optimizations to improve efficiency and user experience.
- Refactoring of Storage/DB Library: Continue the complete refactoring of our storage and database libraries for the blockchain and UTXO Set to enhance data handling and scalability.
- Expand Multilanguage Support: Ensure all our libraries in C++, C, C#, JavaScript, TypeScript, Python, and WebAssembly are updated and compatible with the new changes.

# Objective

The objective of this campaign is to raise 120 BCH to fund the development and execution of the enhancements detailed above over the next six months. These resources will be instrumental in driving Knuth’s progression and ensuring we meet our goals within the planned timeline.

# Role of Knuth in the Bitcoin Cash Community

Knuth has been a steadfast contributor to the Bitcoin Cash ecosystem since its inception. As one of the pioneering nodes, we’ve consistently provided high-performance tools and libraries that empower developers and users alike. Our continuous dedication and unwavering commitment reflect our deep-rooted belief in Bitcoin Cash’s potential and future.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>

# Thank you

We deeply appreciate your support and belief in our work. Together, we can ensure that Bitcoin Cash continues to evolve and meet the growing needs of the global community. Your contribution today is an investment in the future of peer-to-peer electronic cash.
Join us in making Bitcoin Cash stronger—[support our Flipstarter campaign](https://fund.kth.cash/?id=18) now!
