The Knuth engineering team will start working on the first version of the Knuth Javascript library.

Knuth is a Bitcoin Cash full-node written in C++17, but it's not just that.

Our goal is to provide a set of libraries in various programming languages that simplifies application development, ease on-boarding of new developers and let them build their new ideas and really boost the Bitcoin Cash ecosystem.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/js-demo.png" /></p>

So the Javascript library is about to join our family of libraries: [C++17](https://github.com/k-nuth/node), [C](https://github.com/k-nuth/c-api) and [C#](https://github.com/k-nuth/cs-api). But this does not end here, our plan is to provide libraries in other popular programming languages: Rust, Python, Java, Go, Eiffel, ...

<!-- <p align="center"><img width="400px" src="https://raw.githubusercontent.com/k-nuth/logos/master/kth-js.png" /></p> -->

<p align="center" width="100%">
    <img width="400px" src="https://raw.githubusercontent.com/k-nuth/logos/master/kth-js.png" />
</p>

<!-- <img align="center" width="400px" src="https://raw.githubusercontent.com/k-nuth/logos/master/kth-js.png"> -->

Once this project is completed, our next step will be to use this new Javascript library to create our own block explorer on top of it. This other project will serve as an example to demonstrate the full power of Knuth's APIs.

We would like you to be able to use the Knuth node as a library in your favorite programming language.

<p align="center"><img width="800px" src="https://github.com/k-nuth/k-nuth.github.io/raw/master/images/Knuth%20Programming%20langs.png" /></p>

# Requested Funding

The estimated time for the development of the Knuth Javascript library is 240 hours.
At a rate of 0.2 BCH/hr the total requested funding for this phase of development is 48 BCH.

# Knuth's 2020 in numbers

If you want to know what we have been working on for the past year, check out the following article: https://read.cash/@kth/knuths-2020-in-numbers-e900dfc6


# Plan for 2021

- Development of the Knuth Javascript Library (this Flipstarter campaign).
- Create our own block explorer on top of the JS library.
- Improve the performance of the node so that it can adapt to the times that come in Bitcoin Cash: 256 MB blocks. For this we have determined that we will need to (1) rewrite the IDB code from scratch and (2) implement a better concurrency scheme.
- Maintain and improve our existing APIs: C++, C, C#.
- Implement UTXO Commitments.
- Implement Double-Spend Proofs.
- Development of the Knuth Rust Library.
- Development of the Knuth Python Library.
- Development of the Knuth Java Library.
- Development of the Knuth Go-lang Library.
- Development of the Knuth Eiffel Library.

The listing above is our entire backlog. Instead of doing a big campaign with all that, we prefer to go on feature-by-feature campaigns basis.
Once we finish the current one, we will move on to the next one. In this way, it allows us to do shorter tasks and since the Bitcoin Cash ecosystem is very dynamic, we can adapt to the demands of the moment.

# Thank you!

Knuth is open source software that needs community support to continue its development. Therefore we would like to thank the generosity of our supporters and that of the entire Bitcoin Cash ecosystem. You are the ones that allow us to move forward with our goal, which is everyone's goal at Bitcoin Cash, which is to be *the Peer-to-Peer Electronic Cash System* for the entire world.
